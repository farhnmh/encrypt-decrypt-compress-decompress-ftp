# encrypt decrypt-compress decompress-FTP

* **Praktikum Jaringan Komputer Semester 3**
* 4210181002_Farhan Muhammad

* **Description**
1. Before send file, client will compress the file that want send
2. After that, the compressed file will encrypted
3. Client will send the compressed and encrypted file to server
4. After the server received the file, server will decrypt the file
5. Finally, server extract it in specific folder